def print_budget_plan(item, cost):
    # Always write proper docstrings and comments.
    """
    Just a sample function to print the item and its cost.

    :type item: str
    :param item: The item you want to buy

    :type cost: int
    :param cost: The cost of the `item`

    :rtype: None
    :returns: Nothing. Just print a message.
    """

    # `f""` is called `f-string` available in Python 3+  
    print(f"{item} will cost you Rs. {cost}.")

# Search more about `if __name__ == "__main__"` syntax from the Web.
if __name__ == "__main__":
    # Take input from the user 
    item = input("Which item do you want to buy?...")
    cost = input(f"How much will {item} cost you?...")

    item = item.strip()
    # `strip()` is available for all string objects. It strips off any leading and trailing whitespaces.
    
    # Validation: 1
    # Check if the `item` is an empty string.
    if not item:
        print(f"'item' cannot be empty!")
        # Terminate the program!
        exit()

    # Validation: 2
    # Validation using exception handling. Check if `cost` is an integer or not.
    try:
        # Type conversion or casting.
        cost = int(cost)
    # Always specify the exact error you are expecting in the `try` block.
    except ValueError:
        print(f"'cost' must be an integer - not {cost}")
        exit()

    # If the execution has reached this line, that means all validations were passed. All inputs are valid. 
    # So you can call budget function.
    print_budget_plan(item=item, cost=cost)
    # fucntion call by passing `kwargs`. Read more about `args` and `kwargs` on the Web.
